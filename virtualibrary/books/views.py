from django.http import HttpResponse
from django.template import loader

def index(request):
	template = loader.get_template('home.html')
	context = {}
	return HttpResponse(template.render(context, request))

def books(request):
	template = loader.get_template('books.html')
	context = {}
	return HttpResponse(template.render(context, request))

def register(request):
	template = loader.get_template('register.html')
	context = {}
	return HttpResponse(template.render(context, request))

def login(request):
	template = loader.get_template('login.html')
	context = {}
	return HttpResponse(template.render(context, request))

def passwordrecover(request):
	template = loader.get_template('password-recovery.html')
	context = {}
	return HttpResponse(template.render(context, request))