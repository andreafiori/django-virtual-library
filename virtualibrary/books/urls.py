from django.conf import settings
from django.conf.urls import include, url
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('books', views.books, name='books'),
    path('register', views.register, name='register'),
    path('password-recover', views.passwordrecover, name='forgotpassword'),
    path('login', views.login, name='login'),
]

# Add toolbar
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
