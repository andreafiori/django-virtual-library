from django.db import models
from django.contrib.auth.models import AbstractUser

SEX_CHOICES = (
    ('m', 'Male'),
    ('f', 'Female'),
)

class Books(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True, null=True)
    category = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    language = models.CharField(max_length=10, blank=True, null=True)
    editor = models.CharField(max_length=70, blank=True, null=True)
    datepublish = models.DateField(blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    filename = models.FileField(upload_to='vlib')

    class Meta:
        managed = False
        db_table = 'books'
        verbose_name = 'Books'
        verbose_name_plural = 'Books'

class Users(models.Model):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    city = models.CharField(max_length=80, blank=True, null=True)
    district = models.CharField(max_length=10, blank=True, null=True)
    area = models.CharField(max_length=10, blank=True, null=True)
    sex = models.CharField(choices=SEX_CHOICES, default=SEX_CHOICES[0][0], max_length=10)
    birthdate = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
        verbose_name = 'Users'
        verbose_name_plural = 'Users'
