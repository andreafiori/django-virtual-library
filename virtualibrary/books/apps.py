from django.apps import AppConfig

class VlibraryConfig(AppConfig):
    name = 'books'
