from django.contrib import admin
from .models import Books, Users
from django.contrib.admin import SimpleListFilter
from datetime import date
from django.utils.timezone import now

class AgeFilter(SimpleListFilter):
    title = 'Age'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'birthdate'

    def lookups(self, request, model_admin):
        list_tuple = [
            ( '0to20', '0-20' ),
            ( '21to40', '21-40' ),
            ( '41to60', '41-60' ),
            ( '61next', '61 in poi' ),
        ]
        return list_tuple

    # TODO: refactor and add 61 and beyond
    def queryset(self, request, queryset):
        if self.value() == '0to20':
            return self.age_range(queryset, 0, 20)
        elif self.value() == '21to40':
            return self.age_range(queryset, 21, 40)
        elif self.value() == '41to60':
            return self.age_range(queryset, 41, 60)

    # Method to filter by date range
    def age_range(self, queryset, min_age, max_age):
        current = now().date()
        min_date = date(current.year - min_age, current.month, current.day)
        max_date = date(current.year - max_age, current.month, current.day)

        return queryset.filter(birthdate__gte=max_date, birthdate__lte=min_date).order_by("birthdate")

class SexFilter(SimpleListFilter):
    title = 'Sex'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'sex'

    def lookups(self, request, model_admin):
        list_tuple = []
        for user in Users.objects.all().distinct('sex'):
            list_tuple.append((user.sex, user.sex))
        return list_tuple

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(sex=self.value())
        else:
            return queryset

class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', )
    list_editable = ( 'title', 'author' )
    # search_fields = { 'title' }
    list_per_page = 25
    list_filters = ( )

class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'surname', 'email' )
    list_editable = ( 'name', 'surname', 'email' )
    # search_fields = { 'title' }
    list_per_page = 25
    list_filter = ( SexFilter, AgeFilter )

admin.site.register(Users, UserAdmin)
admin.site.register(Books, BookAdmin)
