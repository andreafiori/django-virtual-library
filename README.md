# Virtual library

A Django application to store users and books.

## Installation

Dependencies are in the requirements.txt file.
Install them using [Pip](https://pypi.org/project/pip/): 

    pip install package

## Tools

The following tools has been used for this project:

- Python 3
- Django 2.0
- VirtualEnv
- Twitter Bootstrap 4
- PyCharm IDE
- Django debug toolbar
- PostgreSQL database

The virtual enviroment is not included in this repository.

## Update requirements.txt

    pip freeze > requirements.txt
    
## Run the server

     cd virtualibrary
     python manage.py runserver

## TODO

- Custom user authentication
- Remove one of two user tables
- Custom error pages (404, 400, 500 etc.)
- Migrate user table filters
- Books pdf upload w check about size, file ext
- Register, login, forgot password forms